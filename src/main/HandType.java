package main;

/**
 * Created by michi on 2014/12/17.
 */
public enum HandType {
    PAPER,
    STONE,
    SCISSORS;

}
