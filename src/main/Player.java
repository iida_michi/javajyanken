package main;

import java.util.Random;

/**
 * Created by michi on 2014/12/17.
 */
public class Player {

    private HandType playerHand;
    private int winCount;
    private int drawCount;

    Player(){
        winCount = 0;
        drawCount = 0;
    }

    /**
     * ランダムにじゃんけんで出す手を決めるメソッド
     */
    public void selectHand(){
        int i = new Random().nextInt(3);
        if(i == 0){
            playerHand = HandType.PAPER;
        }else if(i == 1){
            playerHand = HandType.STONE;
        }else {
            playerHand = HandType.SCISSORS;
        }

    }

    public HandType getPlayerHand() {
        return playerHand;
    }

    public int getWinCount() {
        return winCount;
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public void setDrawCount(int drawCount) {
        this.drawCount = drawCount;
    }
}
