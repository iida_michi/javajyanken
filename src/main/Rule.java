package main;

/**
 * Created by michi on 2015/01/09.
 */
public class Rule {

    /**
     * じゃんけんのルールに従って、
     * @param player1hand
     * @param player2hand
     * @return "player1の勝ち"/"player2の勝ち"/"あいこ"のどれかの文字列
     */
    public static String vsResult(HandType player1hand,HandType player2hand){

        final String player1win = "player1の勝ち";
        final String player2win = "player2の勝ち";
        final String draw = "あいこ";

        String resultStr = "";
        if(player1hand == player2hand){
            return draw;
        }

        if(player1hand == HandType.PAPER){
            if(player2hand == HandType.SCISSORS){
                return  player2win;
            }else {
                return  player1win;
            }
        }

        if(player1hand == HandType.STONE){
            if(player2hand == HandType.SCISSORS){
                return player1win;
            }else {
                return player2win;
            }
        }

        if(player1hand == HandType.SCISSORS){
            if(player2hand == HandType.STONE){
                return  player2win;
            }else {
                return  player1win;
            }
        }


        return  resultStr;
    }
}
