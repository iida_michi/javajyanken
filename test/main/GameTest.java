package main;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void testJudgeWinner1() throws Exception {
        assertEquals(Game.judgeWinner(3, 2),"プレイヤー１の勝ち");
    }

    @Test
    public void testJudgeWinner2() throws Exception {
        assertEquals(Game.judgeWinner(3, 4),"プレイヤー2の勝ち");
    }

    @Test
    public void testJudgeWinner() throws Exception {
        assertEquals(Game.judgeWinner(3, 3),"引き分け");
    }
}