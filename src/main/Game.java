package main;

/**
 * Created by michi on 2015/01/14.
 */
public class Game {

    /**
     * プレイヤー２人でfightCountの回数だけじゃんけんを行う。
     * 「fightCountの回数の戦績」と
     * 「勝った回数の多い最終勝者」を標準出力する
     * @param player1_name
     * @param player2_name
     * @param fightCount
     * @return void
     */
    public static void twoPlayerPlayMatch(Player player1_name,Player player2_name,int fightCount) {
        String winner;

        for(int i = 0;i < fightCount;i++){

            String twoPlayerPlayGameResult = twoPlayerPlayGame(player1_name,player2_name);

            System.out.println("----第" + (i+1) + "回戦目-----");
            System.out.println(player1_name.getPlayerHand().toString() + " vs " + player2_name.getPlayerHand().toString());
            System.out.println(twoPlayerPlayGameResult);
            System.out.println();

            if(("player1の勝ち").equals(twoPlayerPlayGameResult)){
                player1_name.setWinCount( player1_name.getWinCount() + 1 );
            }else if(("player2の勝ち").equals(twoPlayerPlayGameResult)){
                player2_name.setWinCount( player2_name.getWinCount() + 1 );
            }else {
                player1_name.setDrawCount(player1_name.getDrawCount() + 1);
            }
        }

        winner = judgeWinner(player1_name.getWinCount(), player2_name.getWinCount());

        System.out.println("-----じゃんけん終了-------");
        System.out.println(player1_name.getWinCount() + "対" + player2_name.getWinCount() + "で" + winner);

    }



    /**
     * プレイヤー２人の勝利数を比較し、勝者を決める
     * @param player1winCount
     * @param player2winCount
     * @return "プレイヤー１の勝ち"/"プレイヤー2の勝ち"/"引き分け"のどれかの文字列
     */
    public static String judgeWinner(int player1winCount, int player2winCount) {
        String winner;

        if(player1winCount > player2winCount){
            winner = "プレイヤー１の勝ち";
        }else if (player1winCount == player2winCount){
            winner = "引き分け";
        }else {
            winner = "プレイヤー2の勝ち";
        }
        return winner;
    }

    /**
     * ２人で１回のじゃんけん勝負を行う
     * @param player1
     * @param player2
     * @return "player1の勝ち"/"player2の勝ち"/"あいこ"のどれかの文字列
     */
    public static String twoPlayerPlayGame(Player player1,Player player2){

        //出す手を選ぶ
        player1.selectHand();
        player2.selectHand();

        return Rule.vsResult(player1.getPlayerHand(), player2.getPlayerHand());

    }
}
