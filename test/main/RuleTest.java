package main;

import org.junit.Test;
import static org.junit.Assert.*;

public class RuleTest {

    Rule r = new Rule();

    @Test
    public void testVsResult_PSC() throws Exception {
        String result = r.vsResult(HandType.PAPER,HandType.SCISSORS);
        assertEquals(result,"player2の勝ち");
    }

    @Test
    public void testVsResult_PP() throws Exception {
        String result2 = r.vsResult(HandType.PAPER,HandType.PAPER);
        assertEquals(result2,"あいこ");
    }

    @Test
    public void testVsResult_PST() throws Exception {
        String result2 = r.vsResult(HandType.PAPER,HandType.STONE);
        assertEquals(result2,"player1の勝ち");
    }

    @Test
    public void testVsResult_STP() throws Exception {
        String result2 = r.vsResult(HandType.STONE,HandType.PAPER);
        assertEquals(result2,"player2の勝ち");
    }

    @Test
    public void testVsResult_STSC() throws Exception {
        String result2 = r.vsResult(HandType.STONE,HandType.SCISSORS);
        assertEquals(result2,"player1の勝ち");
    }
    @Test
    public void testVsResult_SCST() throws Exception {
        String result2 = r.vsResult(HandType.SCISSORS,HandType.STONE);
        assertEquals(result2,"player2の勝ち");
    }

    @Test
    public void testVsResult_SCP() throws Exception {
        String result2 = r.vsResult(HandType.SCISSORS,HandType.PAPER);
        assertEquals(result2,"player1の勝ち");
    }
}