package main;

/**
 * Created by michi on 2014/12/17.
 */
public class Main {

    public static void main(String args[]){

        Player player1 = PlayerFactory.newInstance();
        Player player2 = PlayerFactory.newInstance();

        //fightCountには１試合の対戦回数を指定する
        final int fightCount = 3;

        Game.twoPlayerPlayMatch(player1, player2, fightCount);
    }


}
